# osm2pg - An easy shell script to download and import OSM-Data

osm2pg is a simple shell script to get data from the OpenStreetMap easily into a PostGIS database. The selection of the current osm data is done via a bounding box based on latitude and longitude. All data in the given bounding box will be downloaded and parsed by Osmosis into a new PostGIS database.

### Example usage

sudo bash osm2pg --user=postgres --database=erlangen --boundingbox=10.8995,49.5269,11.1578,49.6434

### Requirements

Please install postgis, osmosis and wget.

$ apt install postgis

$ apt install osmosis

$ apt install wget

### Optional: Test the osmosis requirements 

$ bash osm2pg -r

### Set password for the postgres user

If you start postgresql for the first time, you have to set a new password for the administration user "postgres".

$ sudo -u postgres psql

$ \password postgres  

Enter a new password 

$ \q

Note: It is often recommended to create and use a non-administrative user (How to? See for beginning: https://wiki.ubuntuusers.de/PostgreSQL/)

### Now you can run the script

$ sudo bash osm2pg --user=postgres --database=erlangen --boundingbox=10.8995,49.5269,11.1578,49.6434

Note: The script will create a new postgis database with hstore and postgis extension. All nodes, ways and relations in a given bounding box will be downloaded from the Overpass API and parsed to your database by Osmosis.

### Working with the data

You can use for example pgAdmin4, QGIS, R or Python to connect to the new database and query your data. 

For a quick look you can also use the commandline tool psql:

$ psql -U postgres -d YOURDATABASE -c "SELECT * FROM nodes LIMIT 10;"

### Troubleshooting

If you get the message "FATAL:  Peer authentication failed for user postgres" by running the script the first time you have to change the configuration in pg_hba.conf 

Edit the pg_hba.conf:
$ sudo nano /etc/postgresql/9.5/main/pg_hba.conf

Change the following line:

local all postgres peer

to:

local all postgres md5

Now restart your postgresql server:

$ sudo service postgresql restart

(Thanks to: https://stackoverflow.com/questions/18664074/getting-error-peer-authentication-failed-for-user-postgres-when-trying-to-ge)

